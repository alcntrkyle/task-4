<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        class API {
            function FullName($FullName) {
                echo "Full Name: $FullName <br>";
            }

            function Hobbies($Hobbies) {
                echo "Hobbies: <br>";
                $count = count($Hobbies);
                for ($i = 0; $i < $count; $i++) {
                    echo  $Hobbies[$i] . "<br>";
                }
            }

            function AdditionalInfo($Info) {
                echo "Age: {$Info->age}<br>";
                echo "Email: {$Info->email}<br>";
                echo "Birthday: {$Info->birthday}<br>";
            }
        }

        class Information {
            public $age;
            public $email;
            public $birthday;
        
            public function __construct($age, $email, $birthday) {
                $this->age = $age;
                $this->email = $email;
                $this->birthday = $birthday;
            }
        }

        $API = new API();

        $API->FullName("Kyla Marie D. Alcantara");
        $myArray = array("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"."Programming", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"."Film Photography", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"."Watching TV Series and Movies");
        $API->Hobbies($myArray);
        $Info = new Information(22, "kylealcantara@gmail.com", "June 28, 2001");
        $API->AdditionalInfo($Info); 
    ?>
</body>
</html>